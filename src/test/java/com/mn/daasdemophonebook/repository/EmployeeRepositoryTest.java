package com.mn.daasdemophonebook.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mn.daasdemophonebook.model.Contact;
import com.mn.daasdemophonebook.model.Contact.ContactType;
import com.mn.daasdemophonebook.model.Employee;
import com.mn.daasdemophonebook.repository.EmployeeRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeRepositoryTest {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private TestEntityManager entityManager;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private Employee createTestEmployee(String name) {
		Employee emp = new Employee(name);
		emp.setDepartment("ITBS");
		
		String firstName = name.split(" ")[0].toLowerCase();
		Collection<Contact> contacts = emp.getContacts();
		contacts.add(new Contact(ContactType.EMAIL, firstName + "@t-systems.com"));
		
		return employeeRepository.save(emp);
	}
	
	@Before
	public void createTestData() {
		logger.debug("Creating Test Employees...");
		Arrays.asList("Gipsz Jakab", "Próba Elek").stream().forEach(name -> createTestEmployee(name));
	}
	
	@Test
	public void testRetrieveEmployee() {
		Iterable<Employee> employees = employeeRepository.findAll();
		employees.forEach(name -> logger.debug("Employee: {}", name));
		
		Long employeeId = employees.iterator().next().getId();
		
		Employee employee = employeeRepository.findOneById(employeeId);
		
		assertThat(employee).isNotNull();
		assertThat(employee.getFullName()).isEqualTo("Gipsz Jakab");
	}
	
	@Test
	public void testRetrieveEmployeeByName() {
		List<Employee> employees = employeeRepository.findDistinctByFullNameOrderByFullName("Gipsz Jakab");
		logger.debug("Employee: {}", employees.get(0).getFullName());
		assertThat(employees).hasSize(1);
	}
	
	@Test
	public void testUpdateName() {
		Employee employee = employeeRepository.findOneByFullName("Próba Elek");
		
		assertThat(employee).isNotNull();
		assertThat(employee.getFullName()).isEqualTo("Próba Elek");
		
		long employeeId = employee.getId();
		employee.setFullName("John Doe");
		
		Employee testEmployee = employeeRepository.findOneByFullName("John Doe");
		assertThat(testEmployee).isNotNull();
		
		Employee testEmployee2 = employeeRepository.findOneById(employeeId);
		assertThat(testEmployee2.getFullName()).isEqualTo("John Doe");
	}
	
	@Test
	public void testAddContact() {
		Employee employee = employeeRepository.findOneByFullName("Gipsz Jakab");
		
		assertThat(employee).isNotNull();
		assertThat(employee.getFullName()).isEqualTo("Gipsz Jakab");
		
		long employeeId = employee.getId();
		
		Contact newEmployeeContact = new Contact(ContactType.MOBILE, "+3614565499");
		
		employee = employeeRepository.save(employee);
		employee.getContacts().add(newEmployeeContact);
		
		assertThat(employee.getContacts()).hasSize(2);
		
		Employee testEmployee = employeeRepository.findOneById(employeeId);
		assertThat(testEmployee.getContacts()).hasSize(2);
	}

}

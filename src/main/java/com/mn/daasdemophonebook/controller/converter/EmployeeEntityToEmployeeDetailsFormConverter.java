package com.mn.daasdemophonebook.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.mn.daasdemophonebook.controller.form.EmployeeDetailsForm;
import com.mn.daasdemophonebook.model.Employee;

@Component
public class EmployeeEntityToEmployeeDetailsFormConverter implements Converter<Employee, EmployeeDetailsForm> {
	@Override
	public EmployeeDetailsForm convert(Employee employee) {
		EmployeeDetailsForm form = new EmployeeDetailsForm();
		form.setEmployee(employee);
		return form;
	}
}

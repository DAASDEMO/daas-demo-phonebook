package com.mn.daasdemophonebook.controller.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.mn.daasdemophonebook.controller.form.EmployeeDetailsForm;
import com.mn.daasdemophonebook.model.Employee;

@Component
public class EmployeeDetailsFormToEmployeeEntityConverter implements Converter<EmployeeDetailsForm, Employee> {
	@Override
	public Employee convert(EmployeeDetailsForm src) {
		return src.getEmployee();
	}
}

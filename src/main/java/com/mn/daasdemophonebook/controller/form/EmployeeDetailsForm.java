package com.mn.daasdemophonebook.controller.form;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.mn.daasdemophonebook.model.Employee;

public class EmployeeDetailsForm {
	
	@Valid
	@NotNull
	private Employee employee;
	
	public static enum Action {
		SAVE, BACK, DELETE, ADD_CONTACT
	}
	
	@NotNull
	private Action action;
	
	@Min(0)
	private Integer deleteContactIndex;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Integer getDeleteContactIndex() {
		return deleteContactIndex;
	}

	public void setDeleteContactIndex(Integer deleteContactIndex) {
		this.deleteContactIndex = deleteContactIndex;
	}

}

package com.mn.daasdemophonebook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mn.daasdemophonebook.service.EmployeeService;

@Controller
public class EmployeeListController {
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = "/")
	public String displayEmployeeList(Model model, @RequestParam(required = false) String searchTerm, Pageable pageable) {
		if(pageable.getPageSize() < 1 || pageable.getPageSize() > 10) {
			pageable = new PageRequest(0, 10);
		}
		model.addAttribute("employeesPage", employeeService.searchByName(searchTerm, pageable));
		return "phonebook";
	}
}

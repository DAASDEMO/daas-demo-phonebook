package com.mn.daasdemophonebook.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mn.daasdemophonebook.controller.form.EmployeeDetailsForm;
import com.mn.daasdemophonebook.model.Contact;
import com.mn.daasdemophonebook.model.Employee;
import com.mn.daasdemophonebook.repository.EmployeeRepository;
import com.mn.daasdemophonebook.service.EmployeeService;

@Controller
@RequestMapping(value = "/details.html")
public class EmployeeDetailsController {
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private ConversionService conversionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String displayEmployee(Model model, @RequestParam(required = false) Long employeeId) {
		Employee employee = employeeId != null ? employeeService.getEmployeeDetails(employeeId) : new Employee();
		EmployeeDetailsForm form = conversionService.convert(employee, EmployeeDetailsForm.class);
		model.addAttribute("employeeForm", form);
		return "details";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String createOrUpdateEmployee(@Valid @ModelAttribute("employeeForm") EmployeeDetailsForm employeeDetailsForm, BindingResult bindingResult) {
		if(employeeDetailsForm.getAction() != null) {
			switch(employeeDetailsForm.getAction()) {
			case SAVE:
				if(bindingResult.hasErrors()) {
					bindingResult.reject("details.error.userInput");
					return "details";
				}
				Employee employee = conversionService.convert(employeeDetailsForm, Employee.class);
				try {
					employee = employeeRepository.save(employee);					
				} catch(ObjectOptimisticLockingFailureException e) {
					bindingResult.reject("details.error.concurrentModification");
					return "details";
				}
				break;
			case DELETE:
				employeeRepository.delete(employeeDetailsForm.getEmployee());
				break;
			case BACK:
				break;
			case ADD_CONTACT:
				employeeDetailsForm.getEmployee().getContacts().add(new Contact());
				return "details";
			}
		} else {
			deleteEntry(employeeDetailsForm.getEmployee().getContacts(), employeeDetailsForm.getDeleteContactIndex());
			employeeDetailsForm.setDeleteContactIndex(null);
			
			return "details";
		}
		
		return "redirect:/";
	}
	
	private <T> void deleteEntry(List<T> list, Integer indexOfElementToDelete) {
        if (indexOfElementToDelete != null && indexOfElementToDelete <= list.size()) {
            list.remove((int) indexOfElementToDelete);
        }
    }
	
	@ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleNotExistingEmployee(EmptyResultDataAccessException e) {
        return "error/employee-does-not-exist";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }
}

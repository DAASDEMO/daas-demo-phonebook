package com.mn.daasdemophonebook.service.impl;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mn.daasdemophonebook.model.Employee;
import com.mn.daasdemophonebook.repository.EmployeeRepository;
import com.mn.daasdemophonebook.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Transactional(readOnly = true)
	public Employee getEmployeeDetails(long employeeId) {
		Employee result = employeeRepository.findOneById(employeeId);
		
		//long cSize = result.getContacts().size();
		
		return result;
	}
	
	public Page<Employee> searchByName(String searchTerm, Pageable pageable) {
		Page<Employee> result = null;
		
		if(searchTerm == null || searchTerm.length() == 0) {
			result = employeeRepository.findAllByOrderByFullName(pageable);
		} else {
			searchTerm = searchTerm.trim();
			searchTerm = MessageFormat.format("%{0}%", searchTerm);
			result = employeeRepository.findDistinctByFullName(searchTerm, pageable);
		}
		
		return result;
	}

}
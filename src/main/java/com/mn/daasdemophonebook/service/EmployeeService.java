package com.mn.daasdemophonebook.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mn.daasdemophonebook.model.Employee;

public interface EmployeeService {
	
	Page<Employee> searchByName(String searchTerm, Pageable pageable);
	
	Employee getEmployeeDetails(long employeeId);

}

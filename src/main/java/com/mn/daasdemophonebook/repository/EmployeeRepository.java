package com.mn.daasdemophonebook.repository;

import org.springframework.data.domain.Pageable;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.repository.CrudRepository;

import com.mn.daasdemophonebook.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

	Page<Employee> findAllByOrderByFullName(Pageable pageable);
	
	Page<Employee> findDistinctByFullName(String fullName, Pageable pageable);
	
	List<Employee> findDistinctByFullNameOrderByFullName(String fullName);
	
	@EntityGraph(value = "contact", type = EntityGraphType.LOAD)
	Employee findOneById(Long Id);
	
	Employee findOneByFullName(String fullName);
	
}
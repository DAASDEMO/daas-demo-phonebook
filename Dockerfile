FROM openjdk:8-jdk-alpine
WORKDIR /
ADD target/daas-phonebook.jar daas-phonebook.jar
ENTRYPOINT ["java", "-jar", "/daas-phonebook.jar"]
EXPOSE 8000
